# Castle Messaging Consructor

A non-linear sms/messaging-like interactive fiction games constructor.

# Abstract

A game constructor for creation of interactive fiction games which look like instant messaging with one or several characters and occaisonally providing the player with choices that influence the story flow.
The story consists of "messages" that can be grouped in "Events" (a single linear storyline of the story or conditionals).
The non-linear storytelling is supported by possible arbitrary order of in-game "Events".
The writer can also make use a set of variables, visual inventory and gallery.

The events of the story can be split into multiple files and use a very simple syntax to write.

# Declaration syntax

All variables, designs, etc. used in the story must be declared in index.xml before using. Also index.xml contains additional information on how to manage the story, such as "which EVENT is run when the player starts the game".

```
<root>
  <game name="index.xml demo" first_event='FirstEvent' typing_speed='1000' />
  <designs>
    <design name="Player" image="/images/player_bubble.png" corners="10 10 10 10" align="right" typing="show" />
    <design name="Hero" image="/images/hero_bubble.png" corners="10 10 10 10" align="left" typing="hide" />
    <design name="ServiceMessage" image="/images/empty.png" corners="0 0 0 0" align="center" typing="none" />
  </designs>
  <gallery use_gallery="true" />
  <variables>
    <var name="Phone" item="true" image="/images/phone.png" start_quantity="1" binary="true" />
    <var name="Torch" item="true" image="/images/torch.png" />
    <var name="Luck" />
  </variables>
</root>
```

audio.xml

```
...
```

# Story Syntax

```
MESSAGE "MessageDesign"
MessageText
```

... Keep in mind that a "too-big" message simply won't fit on screen. Large messages are also hard to read. Try to keep messages down to 1-3 short sentences (200-300 characters), jsut as people would normally do while exchanging SMS or posting tweets/toots.

There will be an automatic pause with typing animation added before displaying the message if `typing="hide"` for the requested message design (best for other characters messages).
In case `typing="show"` the typing animation will be shown (best for player's messages).
The duration of the typing effect will be calculated based on the length of the message.
Messages with `typing="none"` will be displayed immediately without any effects (best for miscellaneous service messages).

```
IMAGE "MessageDesign"
RelativeImageURL
```

Works idential to MESSAGE, but instead of MessageText shows an image.

```
EVENT "Label"
```

Starts a new Event which can be referenced by Label using GOTO.

```
IF "Statement" <------ this is instruction value
THEN "EventLabel1" <------ this is instruction content (2 lines)
ELSE "EventLabel2"
```

If Statement is true then executes GOTO to Event with EventLabel1 otherwise executes GOTO to EventLabel2. ELSE is optional. However, keep in mind, that if Statement was false it will simply go on to the next instruction in this case. It's better to specify a jump in such case explicitly.

```
CASE "VariableOrItem"
0: EventLabel0
1: EventLabel1
2: EventLabel2
3: EventLabel3
GOTO "EventLabelDefault"
```

... the GOTO instruction in the end is optional. However, keep in mind, that if CASE didn't handle the case it will simply go on to the next instruction in this case. It's better to specify a jump in such case explicitly.

```
GOTO "EventLabel"
```

Jumps to Event with label "EventLabel".


```
SLEEP "Seconds"
```

Will pause for specified amount of seconds (may be float, like "0.6" seconds). Typing animation is not shown during the SLEEP.

```
INC "VariableOrItem"
DEC "VariableOrItem"
```

Increases/decreases a variable or inventory items count by one. All variables are integer. If a variable is an item, then it cannot have a negative value and removing excessive items in this case is safe and will result in 0. If a variable is not an item, it can have negative values.

```
SOUND "RelativeURL"
```

Plays a sound file at RelativeURL.

```
MUSIC "MusicName"
AMBIENCE "MusicName"
```

MUSIC and AMBIENCE are two music channels, that will loop until stopped or replaced by a different track. Send empty URL (`MUSIC=""`) to stop the music.
Music has to be declared in audio.xml, because large files have to be managed in a special way.
