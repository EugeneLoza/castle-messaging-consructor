{ MIT License }
{}
unit GameStoryInstructions;

{$I game.inc}

interface
uses
  Generics.Collections;

const
  HeaderEvent = 'EVENT';
  HeaderMessage = 'MESSAGE';
  HeaderImage = 'IMAGE';
  //HeaderScript = 'SCRIPT';
  
type
  TInstructionType = (itMessage, itEvent, itImage);
  
type
  TInstruction = record
    InstructionType: TInstructionType;
    Caption, Content: String;
  end;
  PInstruction = ^TInstruction;
  TInstructionsList = specialize TList<TInstruction>;

function InstructionToStr(const AIstruction: TInstructionType): String;
function GetInstruction(const AString: String; out Instruction: TInstruction): Boolean;
implementation

function InstructionToStr(const AIstruction: TInstructionType): String;
begin
  WriteStr(Result, AInstruction);
end;

function GetInstruction(const AString: String; out Instruction: TInstruction): Boolean;
var
  iPos: Integer;
  
  function IsHeader(const AInstructionHeader: String): Boolean;
  begin
    iPos := Pos(AInstructionHeader, AString) > 0;
    Result := iPos;
    if Result then
      iPos += Length(AInstructionHeader) + 1;
  end;
  
begin
  if IsHeader(HeaderEvent) then
  begin
    Result := true;
    Instruction.InstructionType := itEvent;
  else
  if IsHeader(HeaderMessage) then
  begin
    Result := true;
    Instruction.InstructionType := itMessage;
  else
  if IsHeader(HeaderImage) then
  begin
    Result := true;
    Instruction.InstructionType := itImage;
  else
    Result := false;
  if Result then
  begin
    //note, that everything before the header will be ignored
    Instruction.Value := Trim(Copy(AString, iPos, Length(AString)));
    Instruction.Content := '';
  end;
end;

end.

  