{ MIT license }
{ Main unit of the game. Creates and manages messages,
  displayed to the player}
unit StateGame;

{$I game.inc}

interface

uses
  CastleUiState;
  
type
  { Parsed Castle User Interface design }
  TStateDesignBase = class(TUiState)
  strict protected
    //buttons
    //message templates
  public
    procedure Start; override;
  end;
  
type
  { Core state of the game:
    * displays the messages
    * handles player input
    * handles story operation }
  TStateGame = class(TStateDesignBase)
  strict private
    //
  public
    procedure Start; override;
    procedure Stop; override;
    procedure Update(....); override;
  end;
  
implementation

procedure TStateGameBase.Start;
begin
  inherited;
  //Parse the design;
end;

procedure TStateGame.Start;
begin
  inherited;
  //add the events to buttons, etc.;
end;

procedure TStateGame.Stop;
begin
  inherited;
end;

procedure TStateGame.Update(....);
begin
  inherited;
end;

end.