{ MIT License }
{ A single element of story -
  a linear set of instructions that flow without user input }
unit GameStory;

{$I game.inc}

interface
uses
  Classes, Generics.Collections,
  GameInstructions;

type
  { Contains a set of lines, that are executed sequentially }
  TEvent = class(TObject)
  strict private
    Content: TInstructionsList;
  public
    { Add a line to the event }
    procedure Add(const ALine: String);
    
    constructor Create; //override;
    destructor Destroy; override;
  end;
  TEventDictionary = specialize TObjectDictionary<String, TEvent>;

implementation
uses
  SysUtils;
  
procedure TEvent.Add(const ALine: String);
begin
  Content.Add(ALine);
end;

constructor TEvent.Create;
begin
  inherited; //parent constructor is empty
  Content := TInstructionsList.Create;
end;

destructor TEvent.Destroy;
begin
  Content.Free;
  inherited;
end;
