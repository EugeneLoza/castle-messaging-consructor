{ MIT License }
{ Manager of the in-game events, variables
   }
unit GameStory;

{$I game.inc}

interface
uses
  Classes, Generics.Collections,
  GameStoryEvent;
  
const
  CommentSymbol = '//';

type
  { Container with all the story of the game }
  TStory = class(TObject)
  strict private
    StoryEvents: TEventDictionary;
    procedure ParseStoryFile(const FileUrl: String);
  public
    procedure LoadStory;
    constructor Create; //override;
    destructor Destroy; override;
  end;

implementation
uses
  SysUtils,
  CastleDownload;
  
procedure TStory.ParseStoryFile(const FileUrl: String);

  function TrimAll(const Value: String): String;
  var
    CommentStarts: Integer;
  begin
    CommentStarts := Pos(CommentSymbol, Value, [rfFindFirst]);
    if CommentStarts < 0 then
      Result := Value
    else
      Result := Copy(Value, 0, CommentStarts - 1);
    while Pos('  ', Result) > 0 do
      StrReplace('  ', ' ', Result, [rfAll]);
    StrReplace(#9, ' ', Result, [rfAll]);
    Result := Trim(Result);
  end;

var
  FileStream: TFileStream;
  FileStrings: TStringList;
  NewEventName: String;
  NewEvent: TEvent;
  S: String;
  CurrentString: String;
  NewInstruction: TInstruction;
  OldInstruction: PInstruction;
begin
  FileStream := Download(FileUrl);
  Assert(FileStream <> nil);
  FileStrings := FileStream.ReadStrings; //???
  CurrentLine := '';
  OldInstruction := nil;
  for I := S in FileStrings do
  begin
    CurrentLine := TrimAll(S);
    if CurrentLine <> '' then
    begin
      if GetInstruction(CurrentLine, NewInstruction) then
      begin
        if OldInstruction <> nil then
        begin
          //at least sanity checks --- move it to Event.Add
          if ((OldInstruction^.InstructionType = itMessage) or
            (OldInstruction^.InstructionType = itImage)) and
            (OldInstruction^.Value = '') then
              raise EInternalError.CreateFmt('ERROR: Empty instruction value in event %s', [Event.Caption]);
          //...
        end;
        if NewInstruction.InstructionType = itEvent then
        begin
          if Event <> nil then
            Event.Validate;
          Event := TEvent.Create(NewInstruction);
        end else
        begin        
          OldInstruction := @NewInstruction; //todo:NewInstruction is event!
          if Event = nil then
            raise EInternalError.CreateFmt('Received an Instruction, but there is no Event to add it to. Every new story file should have an Event before any other instructions.');
          Event.Add(Instruction);
        end;
      end else
      begin
        if OldInstruction <> nil then
        begin
          if OldInstruction^.Content <> '' then
            OldInstruction^.Content += '<br/>' + CurrentLine
          else
            OldInstruction^.Content := CurrentLine;
        end else
          raise EInternalError.CreateFmt('A text line %s found, but it cannot be attached to any Instruction', [CurrentLine]);
      end;
    end;
  end;
end;

procedure TStory.LoadStory;
var
  StoryFolder: TStringList;
  S: String;
begin
  StoryFolder := ...;
  for S in StoryFolder do
    ParseStoryFile(S);
end;

constructor TStory.Create;
begin
  inherited; //parent constructor is empty
  StoryEvents := TEventDictionary.Create([doOwnsValues]);
end;

destructor TStory.Destroy;
begin
  Content.Free;
  inherited;
end;

end.